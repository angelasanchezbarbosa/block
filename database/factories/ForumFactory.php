<?php

namespace Database\Factories;

use App\Models\Forum;
use Illuminate\Database\Eloquent\Factories\Factory;

class ForumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Forum::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
               'title' => $this->faker->sentence,
               'description' => $this->faker->text,
               'image'=>$this->faker->imageUrl(1280, 720),
               
               'user_id' => rand(1,5),
               'category_id' => rand(7,10),
        ];
    }
}
