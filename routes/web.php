<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('home');
}); */


Route::get('/',  HomeController::class)->name('home');
Route::get('nosotros',  HomeController::class)->name('nosotros');
/* Rutas de curso */
Route::get('course', [ CourseController::class,'index'])->name('course.index');

Route::get('course/create', [ CourseController::class,'create'])->name('course.create');

Route::post('course/', [ CourseController::class,'store'])->name('course.store');

Route::get('course/texto',[CourseController::class,'texto'])->name('course.texto');

Route::get('course/{course}', [ CourseController::class,'show'])->name('course.show');

/* Rutas del post */
Route::get('post', [ PostController::class,'post'])->name('post');

Route::get('post', [ PostController::class,'index'])->name('post.index');

Route::get('post/create', [ PostController::class,'create'])->name('post.create');

Route::post('post/', [ PostController::class,'store'])->name('post.store');

Route::get('post/{post}', [ PostController::class,'show'])->name('post.show');

/* Rutas del foro */
Route::get('forum', [ ForumController::class,'forum'])->name('forum');

Route::get('forum', [ ForumController::class,'index'])->name('forum.index');

Route::get('forum/create', [ ForumController::class,'create'])->name('forum.create');

Route::post('forum/', [ ForumController::class,'store'])->name('forum.store');

Route::get('forum/{post}', [ ForumController::class,'show'])->name('forum.show');

/* Rutas del comentarios */
Route::get('comment', [ CommentController::class,'comment'])->name('comment');

Route::get('comment', [ CommentController::class,'index'])->name('comment.index');

Route::get('comment/create', [ CommentController::class,'create'])->name('comment.create');

Route::post('comment/', [ CommentController::class,'store'])->name('comment.store');

Route::get('comment/{comment}', [ CommentController::class,'show'])->name('comment.show');

/* Ruta de category */
Route::get('category', [ CategoryController::class,'category'])->name('category');

Route::get('category', [ CategoryController::class,'index'])->name('category.index');

Route::get('category/create', [ CategoryController::class,'create'])->name('category.create');

Route::post('category/', [ CategoryController::class,'store'])->name('category.store');

Route::get('category/{category}', [ CategoryController::class,'show'])->name('category.show');


Route::resource('course', CourseController::class);
Route::resource('post',PostController::class);
Route::resource('forum',ForumController::class);
Route::resource('comment',CommentController::class);
Route::resource('category',CategoryController::class);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
