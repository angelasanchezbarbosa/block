@extends('layouts.web')

@section('title','Crear')


@section('content')
<div class="float-right">
    <a href="{{ route('forum.index') }}"><img class="inline"src="../css/src/arrow-left.svg"></a>
</div>
    <section class="content container-fluid">
        <div class="shadow-xl p-10 bg-gray-700 max-w-64 mt-8 rounded">
            <div class="w-full px-3 mr-2 text-center text-gray-800 font-bold text-xl mb-2  bg-gray-600">

                @includeif('partials.errors')

                
                    <div class="card-header">
                        <span class="card-title">Iniciar un Foro</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('forum.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('forum.form')

                        </form>
                    </div>
                
            </div>
        </div>
    </section>
@endsection
