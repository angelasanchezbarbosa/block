@extends('layouts.web')

@section('title','Editar')
{{--     Update Forum
@endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="shadow-xl p-10 bg-gray-700 max-w-64 mt-8 rounded">
            <div class="col-md-12 w-full px-3 mr-2 text-center text-gray-800 font-bold text-xl mb-2  bg-gray-600">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Actualizar Foro</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('forum.update', $forum->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('forum.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
