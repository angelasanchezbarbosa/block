@extends('layouts.web')

@section('title','Comentarios')
{{--     Comment
@endsection --}}

@section('content')
<h1 class="text-center text-white  font-bold text-xl mb-2  bg-gray-900">Comentarios</h1>
<section class="px-4 sm:px-6 lg:px-4 xl:px-6 pt-4 pb-4 sm:pb-6 lg:pb-4 xl:pb-2 space-y-1">
    
    <div class="w-full lg:max-w-full lg:flex">
        <div class="h-48 lg:h-auto lg:w-58 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
        </div>
<div class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-gray-500 rounded-b lg:rounded-b-none lg:rounded-r p-1 flex flex-col justify-between leading-normal">
    
    <div class="mb-8">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Comment') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('comment.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Create New') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        {{-- <th>No</th> --}}
                                        
										<th>User Id</th>
										{{-- <th>Category Id</th>  --}}
										<th>Title</th>
										<th>Description</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($comments as $comment)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $comment->user_id }}</td>
											<td>{{ $comment->category_id }}</td>
											<td>{{ $comment->title }}</td>
											<td>{{ $comment->description }}</td>
                                            <td>{{ $comment->image }}</></td>

                                            <td>
                                                <form action="{{ route('comment.destroy',$comment->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('comment.show',$comment->id) }}"><i class="fa fa-fw fa-eye"></i> Show</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('comment.edit',$comment->id) }}"><i class="fa fa-fw fa-edit"></i> Edit</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
@endsection
