@extends('layouts.web')

@section('title','crear')
 {{--    Create Comment
@endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="shadow-xl p-10 bg-gray-300 max-w-64 mt-8 rounded">
            <div class="w-full px-3 mr-2 text-center text-gray-800 font-bold text-xl mb-2  bg-gray-600">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Comentar</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('comment.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('comment.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
