@extends('layouts.web')

@section('title','Mostrar')
    {{ $comment->name ?? '' }}
{{-- @endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Show Comment</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('comment.index') }}">Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>User Id:</strong>
                            {{ $comment->user_id }}
                        </div>
                        <div class="form-group">
                            <strong>Category Id:</strong>
                            {{ $comment->category_id }}
                        </div>
                        <div class="form-group">
                            <strong>Title:</strong>
                            {{ $comment->title }}
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            {{ $comment->description }}
                        </div>
                        @livewire('comment-component')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
