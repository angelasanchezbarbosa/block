@extends('layouts.web')

@section('title','Crear')
  {{--   Create Course
@endsection --}}

@section('content')
<div class="float-right">
    <a href="{{ route('course.index') }}"><img class="inline"src="../css/src/arrow-left.svg"></a>
</div>
    <section class="content container-fluid">

        <div class="shadow-xl p-10 bg-gray-300 max-w-64 mt-8 rounded">
            <div class="w-full px-3 mr-2 text-center text-gray-800 font-bold text-xl mb-2  bg-gray-600">

                @includeif('partials.errors')

                
                    <div class="card-header">
                        <span class="card-title">Crear Curso</span>
                    </div>
                    <div class="card-body">
                        <form class="p-5" method="POST" action="{{ route('course.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('course.form')

                        </form>
                    </div>
                
            </div>
        </div>
    </section>
@endsection
