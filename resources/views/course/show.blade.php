@extends('layouts.web')

@section('title','Mostrar')
    {{ $course->name ?? '' }}
{{-- @endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Cursos</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('course.index') }}">Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>User Id:</strong>
                            {{ $course->user_id }}
                        </div>
                        <div class="form-group">
                            <strong>Category Id:</strong>
                            {{ $course->category_id }}
                        </div>
                        <div class="form-group">
                            <strong>Title:</strong>
                            {{ $course->title }}
                        </div>
                        <div class="form-group">
                            <strong>Slug:</strong>
                            {{ $course->slug }}
                        </div>
                        <div class="form-group">
                            <strong>Image:</strong>
                            {{ $course->image }}
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            {{ $course->description }}
                        </div>
                        {{-- <div class="form-group">
                            <strong>Last Used At:</strong>
                            {{ $course->last_used_at }}
                        </div> --}}
                        @livewire('course-component')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
