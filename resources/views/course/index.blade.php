@extends('layouts.web')

@section('title','cursos')
{{--     Course
@endsection --}}

@section('content')
<h1 class="text-center text-gray-700  font-bold text-xl mb-2  bg-gray-900">CURSOS</h1>
<section class="px-4 sm:px-6 lg:px-4 xl:px-6 pt-4 pb-4 sm:pb-6 lg:pb-4 xl:pb-2 space-y-1">
    
    <div class="w-full lg:max-w-full lg:flex">
        <div class="h-48 lg:h-auto lg:w-58 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
        </div>
<div class="border-r border-b border-l border-gray-400 lg:border-l-0 lg:border-t lg:border-gray-400 bg-gray-700 rounded-b lg:rounded-b-none lg:rounded-r p-1 flex flex-col justify-between leading-normal">
    
    <div class="mb-8">
        
      <img class="w-full h-64 rounded-b rounded-t mb-4 opacity-50" src="{{ asset('img/curso.jpg') }}">
      <p class="text-gray-400 text-justify">Comparte tus conocimientos, expresa tus ideas y opiniones de temas tecnólogicos, hemos creado este espacio para tí para que consigas todas tus metas.<br>Es fundamental ir marcándose metas para ir creciendo y evolucionando, y no quedarse anclado. Internet es un apoyo imprescindible, donde podremos encontrar prácticamente cualquier respuesta que busquemos. Google nos sacará de muchos apuros asi como tutoriales de youtube y varias guias que te ayudaran a crecer<br>
      abajo encontaras unos links para aprender gratis</p>
    </div>
  <div class="flex items-center">
   
    <p class="text-gray-700"> y luego compartir esos conocimientos</p>
          <ul>
          <li class="flex mb-4 text-gray-400"><img class=bg-purple-500 src="../css/src/arrow-right.svg"><a href="https://www.w3schools.com/"target="_blank">Documentacion</li></a>
          <li class="flex mb-4 text-gray-400"><img class=bg-purple-500src="../css/src/arrow-right.svg"><a href="https://code.org//"target="_blank">Codigo</li></a>
          <li class="flex mb-4 text-gray-400"><img class=bg-purple-500src="../css/src/arrow-right.svg"><a href="https://scratch.mit.edu/"target="_blank">Juegos</li></a>
          </ul>
</div>
</div>
</div>

{{-- creacion y buscador  --}}       
    <button class="hover:bg-light-blue-200 hover:text-light-blue-800 group flex items-center rounded-md bg-light-blue-100 text-light-blue-600 text-sm font-medium px-4 py-2">
      <svg class="group-hover:text-light-blue-600 text-light-blue-500 mr-2" width="12" height="20" fill="currentColor">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M6 5a1 1 0 011 1v3h3a1 1 0 110 2H7v3a1 1 0 11-2 0v-3H2a1 1 0 110-2h3V6a1 1 0 011-1z"/>
      </svg>
       <div class="float-rigth p-2 ">
          <a href="{{ route('course.create') }}"data-placement="rigth">
            {{ __('Crear un Curso') }}
          </a>
        </div>
    </button>
  
  <form class="relative">
    <svg width="20" height="20" fill="currentColor" class="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
    </svg>
    <input class="focus:border-light-blue-500 focus:ring-1 focus:ring-light-blue-500 focus:outline-none w-1/2 text-sm text-black placeholder-gray-500 border border-gray-300 rounded-md py-2 pl-10" type="text" aria-label="Filter projects" placeholder="Filtrar Curso" />
  </form>
  {{-- table --}}
    <div class="container-fluid">
        <div class="row border">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-gray-900 text-white font-bold ">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Todos los Cursos') }}
                            </span>

                            
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    
                        <div class="table-responsive w-full border-gray-500">
                            <table class="min-w-full divide-y divide-gray-400 border bg-gray-300">
                                <thead class="thead">
                                    <tr class="bg-gray-500 text-center">
                                
                                     
                                        
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">User Id</th> 
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Categoría Id</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Titulo</th>
										
										
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Descripción</th>
                                        <th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Imagen</th> 
                                        <th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Acciones</th>
										

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center w-full lg:max-w-full  lg:divide-y divide-gray-400">
                                    @foreach ($course as $course)
                                        <tr >
                                            {{-- <td>{{ ++$i }}</td> --}} 
                                            
										   <td>{{ $course->user_id }}</td>
											<td>{{ $course->category_id }}</td>
											<td>{{ $course->title }}</td>
											
											<td>{{ $course->image }}</td>
											<td>{{ $course->description }}</td>
											
                                            
                                            <td>
                                                <form action="{{ route('course.destroy',$course->id) }}" method="POST">
                                                    <a  href="{{ route('course.show',$course->id) }}"class="text-green-500 hover:text-purple-700 mr-4"><img class="inline"src="../css/src/eye.svg">Mostrar</a>
                                                    <a  href="{{ route('course.edit',$course->id) }}"class="text-blue-500 hover:text-purple-700 ml-8"><img class="inline"src="../css/src/document.svg">Editar</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="text-red-500" type="submit"><img class="inline" src="../css/src/trash.svg">Borrar</button>
                                                </form>
                                            </td>
                                           
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection
