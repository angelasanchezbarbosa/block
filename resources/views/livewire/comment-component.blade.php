<div>
    <div>
        <div>
            @section('content')
            <h1 class="text-center text-white  font-bold text-xl mb-2  bg-gray-900">FOROS</h1>
          
               <div class="">
                @foreach ($comments as $comment)
        
                <x-comment-card :comment="$comment" />
                @endforeach
        
        </div>
        @endsection
          
        
        </div>
    </div>
    
</div>
