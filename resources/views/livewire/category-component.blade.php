@extends('layouts.web')

<div>
    @section('content')
    <h1 class="text-center text-white  font-bold text-xl mb-2  bg-gray-900">CATEGORIAS</h1>
  
       <div class="">
        @foreach($category  as $category)

        <x-category-card :category="$category" />
        @endforeach


</div>
@endsection
  



