<div>
    <div>
        <div>
            <div>
                @section('content')
                <h1 class="text-center text-white  font-bold text-xl mb-2  bg-gray-900">CURSOS</h1>
              
                   <div class="">
                    @foreach ($courses as $course)
            
                    <x-course-card :course="$courses" />
                    @endforeach
            
            </div>
            @endsection
              
            
            </div>
        </div>
        
    </div>
    
</div>
