<div>
    <div>
        @section('content')
        <h1 class="text-center text-white  font-bold text-xl mb-2  bg-gray-900">FOROS</h1>
      
           <div class="">
            @foreach ($forum as $forum)
    
            <x-forum-card :forum="$forum" />
            @endforeach
    
    </div>
    @endsection
      
    
    </div>
</div>
