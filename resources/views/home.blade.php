@extends('layouts.web')
@section('title','home')

@section('content') 


<body class="bg-gray-100">
<div class="bg-cover bg-center text-center " style="height:32rem; background-image: url(https://images.unsplash.com/photo-1504384308090-c894fdcc538d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80);">
      <div class="flex items-center text-gray-500 font-bold text-center xl justify-center h-full max-w-7xl bg-gray-900 bg-opacity-50">¡CODIFICANDO EL FUTURO!</div>


<div class="min-h-screen flex flex-col flex-auto flex-shrink-0 antialiased bg-gray-50 text-gray-800">
  <div class="fixed flex flex-col top-0 left-0 w-64 bg-gray-900 h-full shadow-lg">
      <div class="flex items-center pl-6 h-20 border-b border--gray-100">
        <div class="flex flex-row items-center h-8"><img class=" bg-gray-500 inline" src="../css/src/user-circle.svg">
          <div class="flex font-semibold text-sm text-gray-300 my-4 font-sans uppercase p-2"><a href="{{ url('/dashboard') }}">CATEGORIAS</div>
      </div>
      </div>
      <div class="overflow-y-auto overflow-x-hidden flex-grow">
        
      <ul class="flex flex-col py-6 space-y-1">
          
      
          <li class="flex flex-row items-center h-8">
              <a href="{{ route('home')}}"class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-700 text-gray-500 hover:text-gray-200 border-l-4 border-transparent hover:border-blue-500 pr-6"><img class="inline-flex justify-center items-center ml-4" src="../css/src/home.svg"><span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">Inicio</span>
              </a>
          </li>
          <li>
            <div class= "group inline-block">
              <li class="flex flex-row items-center h-8 ">
                <a href="{{route('course.index')}}"class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-700 text-gray-500 hover:text-gray-200 border-l-4 border-transparent hover:border-blue-500 pr-6"><img class="inline-flex justify-center items-center ml-4" src="../css/src/archive.svg"><span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">Cursos</span>
                      <li class="rounded-sm px-3 py-2 hover:bg-gray-100"><a href="{{route('forum.index')}}" class="block mx-4 mt-2 md:mt-0 text-sm text-gray-700 capitalize hover:text-blue-600">Web developers</a></li>
                      <li class="rounded-sm px-3 py-1 hover:bg-gray-100"><a href="{{route('forum.index')}}" class="block mx-4 mt-2 md:mt-0 text-sm text-gray-700 capitalize hover:text-blue-600">Web Designers</a></li>
                      <li class="rounded-sm px-3 py-1 hover:bg-gray-100"><a href="{{route('forum.index')}}" class="block mx-4 mt-2 md:mt-0 text-sm text-gray-700 capitalize hover:text-blue-600">UI/UX Designers</a></li>
                      <li class="rounded-sm px-3 py-1 hover:bg-gray-100"><a href="{{route('nosotros')}}" class="block mx-4 mt-2 md:mt-0 text-sm text-gray-700 capitalize hover:text-blue-600">Contacto</a></li>
                </a>
              </li>
            
            
          <li class="flex flex-row items-center h-8">
            <a href="{{route('post.index')}}"class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-700 text-gray-500 hover:text-gray-200 border-l-4 border-transparent hover:border-blue-500 pr-6"><img class="inline-flex justify-center items-center ml-4" src="../css/src/users.svg"><span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">Post</span>
            </a>
        </li> 
        <li class="flex flex-row items-center h-8">
          <a href="{{route('forum.index')}}"class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-700 text-gray-500 hover:text-gray-200 border-l-4 border-transparent hover:border-blue-500 pr-6"><img class="inline-flex justify-center items-center ml-4" src="../css/src/globe.svg"><span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">Foros</span>
          </a>
      </li> 
      <li class="flex flex-row items-center h-8">
        <a href="{{route('comment.index')}}"class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-700 text-gray-500 hover:text-gray-200 border-l-4 border-transparent hover:border-blue-500 pr-6"><img class="inline-flex justify-center items-center ml-4" src="../css/src/hand.svg"><span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">Comentarios</span>
        </a>
    </li>  
           
          <li class="px-5">
          <div class="flex flex-row items-center h-8">
              <div class="flex font-semibold text-sm text-gray-300 my-4 font-sans uppercase">Settings</div>
          </div>
          </li>
          <li>
          <a href="{{ route('login') }}" class="relative flex flex-row items-center h-11 focus:outline-none hover:bg-gray-700 text-gray-500 hover:text-gray-200 border-l-4 border-transparent hover:border-blue-500 pr-6">
              <span class="inline-flex justify-center items-center ml-4">
              <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z"></path></svg>
              </span>
              <span class="ml-2 font-semibold text-sm tracking-wide truncate font-sans">Perfil</span>
          </a>
          </li>
          </ul>
        </div>
      </div>
  
</div>
</div>
</div>
<!-- divide -->
<hr class="my-6 "> 
{{-- card --}}

    <div class="max-w-4xl mx-auto sm:px-6 lg:px-8 mr-12">
    <div class="overflow-hidden shadow-md">
        <!-- card header -->
        <div class="px-6 py-4 bg-gray-800 border-b border-gray-200 font-bold uppercase text-gray-300">
            Web developers
        </div>

        <!-- card body -->
        <div class="p-6 bg-white border-b border-gray-400">
            <!-- content goes here -->
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type 
            specimen book.
        </div>

        <!-- card footer -->
        <div class="p-6 bg-white border-gray-200 text-right">
            <!-- button link -->
            <a class="bg-gray-700 shadow-md text-sm text-white font-bold py-3 md:px-8 px-4 hover:bg-blue-400 rounded uppercase" 
                href="{{route('course.index')}}">Click Aca</a>
        </div>
    </div>
</div>

<!-- divide -->
<hr class="my-6"> 

<!-- dark mode -->
<div class="max-w-4xl mx-auto sm:px-6 lg:px-8 mr-12">
    <div class="overflow-hidden shadow-md text-gray-100">
        <!-- card header -->
        <div class="px-6 py-4 bg-gray-800 border-b border-gray-600 font-bold uppercase">
            Seminarios
        </div>

        <!-- card  body -->
        <div class="p-6 bg-gray-800 border-b border-gray-600">
            <!-- content goes here -->
            Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, 
            when an unknown printer took a galley of type and scrambled it to make a type 
            specimen book.
        </div>

        <!-- card footer -->
        <div class="p-6 bg-gray-800 border-gray-200 text-right">
            <!-- button link -->
            <a class="bg-gray-700 shadow-md text-sm text-white font-bold py-3 md:px-8 px-4 hover:bg-blue-400 rounded uppercase" 
                href="{{route('post.index')}}">Click Aca</a>
        </div>
    </div>
</div>
    
        
  
   </body>
   <!-- divide -->
   <hr class="my-6">  
  <footer>
    <section class="bg-gray-700 py-8 w-full border-white">
      <div class="container mx-auto px-8">
          <div class="table w-full">
              <div class="block sm:table-cell">
                  <p class="uppercase text-grey text-sm sm:mb-6">Links</p>
                  <ul class="list-reset text-xs mb-6">
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">preguntas frecuentes</a>
                      </li>
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Ayuda</a>
                      </li>
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Soporte</a>
                      </li>
                  </ul>
              </div>
              <div class="block sm:table-cell">
                  <p class="uppercase text-grey text-sm sm:mb-6">Legal</p>
                  <ul class="list-reset text-xs mb-6">
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Terminos</a>
                      </li>
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Privacidad</a>
                      </li>
                  </ul>
              </div>
              <div class="block sm:table-cell">
                  <p class="uppercase text-grey text-sm sm:mb-6">Social</p>
                  <ul class="list-reset text-xs mb-6">
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Facebook</a>
                      </li>
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Linkedin</a>
                      </li>
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Twitter</a>
                      </li>
                  </ul>
              </div>
              <div class="block sm:table-cell">
                  <p class="uppercase text-grey text-sm sm:mb-6">La Compañia</p>
                  <ul class="list-reset text-xs mb-6">
                      
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="{{route('nosotros')}}" class="text-grey hover:text-grey-dark">Nosotros</a>
                        </li>
                      <li class="mt-2 inline-block mr-2 sm:block sm:mr-0">
                          <a href="#" class="text-grey hover:text-grey-dark">Contactanos</a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </section>
  </footer>

    
     
      
        

 
@endsection

