@extends('layouts.web')

@section('title','Editar')
{{--     Update Post
@endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="shadow-xl p-10 bg-gray-300 max-w-64 mt-8 rounded">
            <div class="col-md-12 w-full px-3 mr-2 text-center text-gray-800 font-bold text-xl mb-2  bg-gray-600">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">Actualizar Post</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('post.update', $post->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('post.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
