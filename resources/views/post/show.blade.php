@extends('layouts.web')

@section('title','Mostar')
    {{ $post->name ?? '' }}
{{-- @endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">Post</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('post.index') }}">Volver</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>User Id:</strong>
                            {{ $post->user_id }}
                        </div>
                        <div class="form-group">
                            <strong>Category Id:</strong>
                            {{ $post->category_id }}
                        </div>
                        <div class="form-group">
                            <strong>Title:</strong>
                            {{ $post->title }}
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            {{ $post->description }}
                        </div>
                        <div class="form-group">
                            <strong>Image:</strong>
                            {{ $post->image }}
                        </div>
                        @livewire('post-component')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
