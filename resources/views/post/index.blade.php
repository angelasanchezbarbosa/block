@extends('layouts.web')

@section('title','Post')
 {{--    Post
@endsection --}}

@section('content')
<h1 class="text-center text-gray-500  font-bold text-xl mb-2 bg-gray-900">POST</h1>

<div class="w-2xl p-2 lg:max-w-full lg:flex">
    <div class="h-48 lg:h-auto lg:w-58 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
    </div>
<div class="border-r border-b border-l border-gray-500 lg:border-l-0 lg:border-t lg:border-gray-400 bg-gray-500 rounded-b lg:rounded-b-none lg:rounded-r p-4 flex flex-col justify-between leading-normal">
   <div class="mb-8">
<div class="flex items-center">
    <img class="w-full h-64 {{-- rounded-full --}} mr-4" src="{{ asset('img/post.jpeg') }}">
    </div>

   
      <div class="text-gray-700 font-bold text-xl text-center mb-2"></div>
       <p class="text-gray-700 text-base"><p> ipsum dolor sit amet consectetur, adipisicing elit. Sequi porro quia quis. Magnam illum corporis eius! Doloribus impedit modi quis distinctio suscipit, ducimus laudantium, eos, nam sint possimus odit quo.Tus publicaciónes, Son  notas o mensajes de texto que se publican en una web social (blog, Facebook, página web…) y que pueden ir acompañado   </p>
       </div>
       
</div>
</div>
{{-- crear y buscar --}}
<button class="hover:bg-light-blue-200 hover:text-light-blue-800 group flex items-center rounded-md bg-light-blue-100 text-light-blue-600 text-sm font-medium px-4 py-2">
    <svg class="group-hover:text-light-blue-600 text-light-blue-500 mr-2" width="12" height="20" fill="currentColor">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M6 5a1 1 0 011 1v3h3a1 1 0 110 2H7v3a1 1 0 11-2 0v-3H2a1 1 0 110-2h3V6a1 1 0 011-1z"/>
    </svg>
     <div class="float-rigth p-2 ">
        <a href="{{ route('post.create') }}"data-placement="rigth">
          {{ __('Crear un Post') }}
        </a>
      </div>
  </button>
  <form class="relative">
    <svg width="20" height="20" fill="currentColor" class="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400">
      <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
    </svg>
    <input class="focus:border-light-blue-500 focus:ring-1 focus:ring-light-blue-500 focus:outline-none w-1/2 text-sm text-black placeholder-gray-500 border border-gray-300 rounded-md py-2 pl-10" type="text" aria-label="Filter projects" placeholder="Filtrar Curso" />
  </form>
  {{-- tabla --}}
  <div class="container-fluid">
    <div class="row border">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header bg-gray-900 text-gray-500 font-bold p-2 m-2  ">
                    <div style="display: flex; justify-content: space-between; align-items: center;">

                        <span id="card_title">
                            {{ __('Todos los Post') }}
                        </span>

                        
                    </div>
                </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    
                        <div class="table-responsive md:grid-rows-6 border-gray-900 p-2">
                            <table class="min-w-full divide-y divide-gray-400 border bg-gray-300">
                                <thead class="thead">
                                    <tr class="bg-gray-500 text-center">
                                        <th>No</th>
                                       
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Usuario Id</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Categoria Id</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Titulo</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Descripción</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Imagen</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center w-full lg:max-w-full  lg:divide-y divide-gray-400">
                                    @foreach ($post as $post)
                                        <tr>
                                            {{-- <td>{{ ++$i }}</td> --}}
                                            
											<td>{{ $post->user_id }}</td>
											<td>{{ $post->category_id }}</td>
											<td>{{ $post->title }}</td>
											<td>{{ $post->description }}</td>
											<td>{{ $post->image }}</td>

                                            <td>
                                                <form action="{{ route('post.destroy',$post->id) }}" method="POST">
                                                    <a href="{{ route('post.show',$post->id) }}"class="text-green-500 hover:text-purple-700 mr-4"><img class="inline"src="../css/src/eye.svg">Mostrar</a>
                                                    <a href="{{ route('post.edit',$post->id) }}"class="text-blue-500 hover:text-purple-700 ml-8"><img class="inline"src="../css/src/document.svg">Editar</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="text-red-500" type="submit"><img class="inline" src="../css/src/trash.svg">Borrar</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
             
            </div>
        </div>
    </div>
@endsection
