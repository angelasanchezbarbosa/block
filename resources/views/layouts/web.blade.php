
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link href="{{asset('css/app.css')}}" rel="stylesheet">
  <!-- Tailwind CSS -->
  <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
@livewireStyles
  <title>@yield('title')</title>
  

</head>
<header class="shadow-xl">
  <div class="bg-white py-1"></div>
  
  <nav class="bg-gray-900 py-1 shadow"> <div class="container mx-auto px-6 py-3 ">
    
   
     <a href="{{ route('dashboard')}}"> 
     

          <img
           src="{{ asset('img/logo.png') }}" 
           class="rounded h-12 mx-rigth">
      </a> 
      <div class="bg-white py-1"></div> 
           
     
@if (Route::has('login'))

<div class="hidden fixed top-0 right-0 px-6 py-4 sm:block">
 
<a href="{{ route('home')}}"class=" text-xl font-semibold text-gray-700 underline mr-6 ">INICIO</a> 

@auth

<a href="{{ url('/dashboard') }}" class="text-xl font-semibold text-gray-700 underline ">CATEGORIAS</a>

@else

<a href="{{ route('login') }}" class="text-xl font-semibold text-gray-700 underline p-4">INICIAR SESION</a>

@if (Route::has('register'))
<a href="{{ route('register') }}" class="text-xl font-semibold text-gray-700 underline">REGISTRATE</a>

@endif

@endauth

</div>
@endif
</nav>
</header>

<body>
  @yield('content')



@livewireScripts
</body>
</html>




     

   





