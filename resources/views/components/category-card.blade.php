
  <div>
    <a href="{{route('category.index', $category)}}"></a> 
  <div>
    {{--   regresar --}}
    <div class="float-right">
        <a href="{{ route('category.index') }}"><img class="inline"src="../css/src/arrow-left.svg"></a>
    </div>
   
 <div>
    <div class="bg-gray-100 lg:py-12 lg:flex lg:justify-center">
        <div class="bg-white lg:mx-8 lg:flex lg:max-w-5xl lg:shadow-lg lg:rounded-lg">
            <div class="lg:w-1/2">
                <div class="h-64 bg-cover lg:rounded-lg lg:h-full" style="background-image"><img src={{ $category->image }}></div>
            </div>
            <div class="py-12 px-6 max-w-xl lg:max-w-5xl lg:w-1/2">
     {{--            <p class="mt-4 text-gray-600">{{ $category->id }}</p> --}}
                <h2 class="text-3xl text-gray-800 font-bold">{{ $category->title }}<span class="text-indigo-600">Categoría</span></h2>
                <p class="mt-4 text-gray-600">{{ $category->description }}</p>
                
            </div>
        </div>
    </div> 
</div>