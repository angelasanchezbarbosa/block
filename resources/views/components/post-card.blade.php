<div>
    <a href="{{route('post.index', $post)}}"></a> 
  <div>
    <div class="float-right">
        <a href="{{ route('post.index') }}"><img class="inline"src="../css/src/arrow-left.svg"></a>
    </div>
     <div class="bg-gray-100 lg:py-12 lg:flex lg:justify-center">
        <div class="bg-gray-400 lg:mx-8 lg:flex lg:max-w-5xl lg:shadow-lg lg:rounded-lg">
            <div class="lg:w-1/2">
                <div class="h-64 bg-cover lg:rounded-lg lg:h-full" style="background-image"><img src={{ $post->image }}></div>
            </div>
            <div class="py-12 px-6 max-w-xl lg:max-w-5xl lg:w-1/2">
                <h2 class="text-3xl text-gray-800 font-bold">{{ $post->title }}<span class="text-indigo-600">Post</span></h2>
  
                <p class="mt-4 text-gray-600">{{ $post->description }}</p>
                <div class="mt-8">
                    <a href="#" class="bg-gray-900 text-gray-100 px-5 py-3 font-semibold rounded">Empieza Ahora</a>
                </div>
            
        
    
                <button class="flex-none flex items-center justify-center w-12 h-12 text-gray-900 border border-gray-200" type="button" aria-label="like">
                    <svg width="20" height="20" fill="currentColor">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z" />
                    </svg>
                </button> 
            </div>
         </div>
    </div>
  </div>
  