@extends('layouts.web')

@section('title','Categorias')



@section('content')
<h1 class="text-center text-white  font-bold text-xl mb-2 bg-gray-900">CATEGORIAS</h1>
<section class="px-4 sm:px-6 lg:px-4 xl:px-6 pt-4 pb-4 sm:pb-6 lg:pb-4 xl:pb-6 space-y-4">
    <header class="justify-between">
        <div class="w-full lg:max-w-full lg:flex">
            <div class="h-48 lg:h-auto lg:w-58 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden">
            </div>
            <img class="w-full h-64 rounded-b rounded-t mb-4 " src="{{ asset('img/categoria.jpg') }}">    
        
      <button class="hover:bg-light-blue-200 hover:text-light-blue-800 group flex items-center rounded-md bg-light-blue-100 text-light-blue-600 text-sm font-medium px-4 py-2">
        <svg class="group-hover:text-light-blue-600 text-light-blue-500 mr-2" width="12" height="20" fill="currentColor">
          <path fill-rule="evenodd" clip-rule="evenodd" d="M6 5a1 1 0 011 1v3h3a1 1 0 110 2H7v3a1 1 0 11-2 0v-3H2a1 1 0 110-2h3V6a1 1 0 011-1z"/>
        </svg>
         <div class="float-right p-2 bg-gray-900 text-gray-200 ">
            <a href="{{ route('category.create') }}"data-placement="left">
              {{ __('Crear una Categoria') }}
            </a>
          </div>
      </button>
    </header>
    <form class="relative">
      <svg width="20" height="20" fill="currentColor" class="absolute left-3 top-1/2 transform -translate-y-1/2 text-gray-400">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" />
      </svg>
      <input wire:model= " search" class=" form-control focus:border-light-blue-500 focus:ring-1 focus:ring-light-blue-500 focus:outline-none w-full text-sm text-black placeholder-gray-500 border border-gray-300 rounded-md py-2 pl-10" type="text" aria-label="Filter projects" placeholder="Filtrar categoria" />
    </form>
    <div class="container-fluid ">
        <div class="row border">
            <div class="col-sm-12">
                <div class="card shadow-2xl">
                    <div class="card-header text-center  rounded-full font-bold text-xl mb-8">
           
                    
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body  grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1 xl:grid-cols-1 md-8 gap-4">
                        <div class="table-responsive md:grid-rows-6 border-gray-900">
                            <table class="min-w-full divide-y divide-gray-900 border bg-gray-500 ">
                                <thead class="thead ">
                                    <tr>
                                        {{-- <th>No</th> --}}
                                        <th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Id</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">titulo</th>
										<th scope="col" class="px-6 py-3 text-center text-l font-bold  text-gray-900 uppercase tracking-wider">Descripción</th>
										<th scope="col" class="px-6 py-3 text-left text-l font-bold  text-gray-900 uppercase tracking-wider">Imagen</th>
										{{-- <th>Last Used At</th> --}}

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center w-full lg:max-w-full  lg:divide-y divide-gray-400">
                                    @foreach ($category as $category)
                                    <div class="bg-gray-200 shadow-lg rounded-lg  text-center ">
                                        <tr>
                                           
                                            <td>{{ $category->user_id }}</td>
											<td>{{ $category->title }}</td>
											<td>{{ $category->description }}</td>
											<td>{{ $category->image }}</td>
											{{-- <td>{{ $category->last_used_at }}</td> --}}
                                             
                                            <td>
                                                <form action="{{ route('category.destroy',$category->id) }}" method="POST">
                                                    <a  href="{{ route('category.show',$category->id) }}"class="text-green-500 hover:text-purple-700"><img class="inline"src="../css/src/eye.svg">Mostrar</a>
                                                    <a href="{{ route('category.edit',$category->id) }}"class="text-blue-500 hover:text-purple-700"><img class="inline"src="../css/src/document.svg">Editar</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="text-red-500" type="submit"><img class="inline" src="../css/src/trash.svg">Borrar</button>
                                                </form>
                                            </td>
                                             </div>
                                        </tr>
                                    </div>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
   
@endsection

