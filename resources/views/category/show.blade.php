@extends('layouts.web')

@section('title','Mostrar')
    {{ $category->name ?? '' }}
{{-- @endsection --}}

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="">
                            <h1 class="text-center text-white p-2 font-bold text-xl mb-2 bg-gray-900">Categorias</h1>
                        </div>
                        <div class="float-right">
                            <a class="bg-purple-600 text-white text-xl font-bold" href="{{ route('category.index') }}"><img class="inline"src="../css/src/arrow-left.svg"> Regresar</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Title:</strong>
                            {{ $category->title }}
                        </div>
                        <div class="form-group">
                            <strong>Description:</strong>
                            {{ $category->description }}
                        </div>
                        <div class="form-group">
                            <strong>Image:</strong>
                            {{ $category->image }}
                        </div>
                       {{--  <div class="form-group">
                            <strong>Last Used At:</strong>
                            {{ $category->last_used_at }}
                        </div> --}}
                        @livewire('category-component')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
