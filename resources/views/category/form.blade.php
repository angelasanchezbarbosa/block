<div class="box box-info padding-1">
    <div class="box-body py-8 mx-auto max-w-screen-xl  sm:flex flex-grow-0 sm:p-5">
        <div class="form-group">
            {{ Form::label('user_id') }}
            {{ Form::text('user_id', $category->user_id, ['class' => 'form-control' . ($errors->has('user_id') ? ' is-invalid' : ''), 'placeholder' => 'User Id']) }}
            {!! $errors->first('user_id', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group ml-12">
            {{ Form::label('Titulo') }}
            {{ Form::text('title', $category->title, ['class' => 'form-control' . ($errors->has('title') ? ' is-invalid' : ''), 'placeholder' => 'Titulo']) }}
            {!! $errors->first('title', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Descripción') }}
            {{ Form::text('description', $category->description, ['class' => 'form-control' . ($errors->has('description') ? ' is-invalid' : ''), 'placeholder' => 'Descripción']) }}
            {!! $errors->first('description', '<div class="invalid-feedback">:message</p>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Imagen') }}
            {{ Form::text('image', $category->image, ['class' => 'form-control' . ($errors->has('image') ? ' is-invalid' : ''), 'placeholder' => 'Imagen']) }}
            {!! $errors->first('image', '<div class="invalid-feedback">:message</p>') !!}
        </div>
     {{--    <div class="form-group">
            {{ Form::label('last_used_at') }}
            {{ Form::text('last_used_at', $category->last_used_at, ['class' => 'form-control' . ($errors->has('last_used_at') ? ' is-invalid' : ''), 'placeholder' => 'Last Used At']) }}
            {!! $errors->first('last_used_at', '<div class="invalid-feedback">:message</p>') !!}
        </div> --}}

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">Enviar</button>
    </div>
</div>