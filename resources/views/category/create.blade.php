@extends('layouts.web')

@section('title','crear')


@section('content')
    <section class="content container-fluid">

        <div class="shadow-xl p-10 bg-gray-300 max-w-64 mt-8 rounded">
            <div class="w-full px-3 mr-2 text-center text-gray-800 font-bold text-xl mb-2  bg-gray-600">

                @includeif('partials.errors')

              
                    <div class="card-header ">
                        <span class="card-title">Crear una Categoria</span>
                    </div>
                    <div class="card-body">
                        <form class="p-5 " method="POST" action="{{ route('category.store') }}"  role="form" enctype="multipart/form-data">
                            @csrf

                            @include('category.form')
                            
                        </form>
                    </div>
                    
            </div>
        </div>
    </section>
@endsection
