<?php

namespace App\Http\Controllers;

use App\Models\Forum;
use Illuminate\Http\Request;

/**
 * Class ForumController
 * @package App\Http\Controllers
 */
class ForumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $forums = Forum::paginate();

        return view('forum.index', compact('forums'))
            ->with('i', (request()->input('page', 1) - 1) * $forums->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $forum = new Forum();
        return view('forum.create', compact('forum'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Forum::$rules);

        $forum = Forum::create($request->all());

        return redirect()->route('forum.index')
            ->with('success', 'Foro creado con éxito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $forum = Forum::find($id);

        return view('forum.show', compact('forum'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $forum = Forum::find($id);

        return view('forum.edit', compact('forum'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Forum $forum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forum $forum)
    {
        request()->validate(Forum::$rules);

        $forum->update($request->all());

        return redirect()->route('forums.index')
            ->with('success', 'Forum updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $forum = Forum::find($id)->delete();

        return redirect()->route('forums.index')
            ->with('success', 'Forum deleted successfully');
    }
}
