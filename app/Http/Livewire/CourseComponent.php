<?php

namespace App\Http\Livewire;
use App\Models\Course;
use Livewire\Component;

class CourseComponent extends Component
{
    public function render()
    {
        return view('livewire.course-component',[
          'course' =>Course::all(),
        ]);
    }
}
