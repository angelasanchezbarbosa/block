<?php

namespace App\Http\Livewire;
use App\Models\Forum;
use Livewire\Component;

class ForumsComponent extends Component
{
    public function render()
    {
        return view('livewire.forums-component',[
            'forum' => Forum::all(),
        ]);
    }
}
