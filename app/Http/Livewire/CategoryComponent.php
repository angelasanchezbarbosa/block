<?php

namespace App\Http\Livewire;
use App\Models\Category;

use Livewire\Component;

class CategoryComponent extends Component


{
   public $search;


    public function render()
    {

        $category = Category::where('user_id',auth()->user()->id)
                            ->where('name','LIKE','%', $this->search);
                            
        return view('livewire.category-component',[
            'category' => Category::all(),
        ]);
    }
}
