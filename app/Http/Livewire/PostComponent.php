<?php

namespace App\Http\Livewire;
use App\Models\Post;

use Livewire\Component;

class PostComponent extends Component
{
    public function render()
    {
         return view('livewire.post-component',[
            'post' => Post::all(),
        ]);

    }
}
