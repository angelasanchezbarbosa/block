<?php

namespace App\Http\Livewire;
use App\Models\Comment;
use Livewire\Component;

class CommentComponent extends Component
{
    public function render()
    {
        return view('livewire.comment-component',[
       'comment' => Comment::all(),
       ]);
   
    }

}
