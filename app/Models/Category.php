<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Course
 *
 * @property $id
 * @property $user_id
 * @property $title
 * @property $description
 * @property $image
 * @property User $user
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */


class Category extends Model
{
    use HasFactory;
    static $rules = [
       
        'title' => 'required',
        'description'=> 'required',
        'imagen'=> 'required',
    ];

    protected $perPage = 20;
 
    protected $fillable =[
        'user_id',
        'title',
        'description',
        'imagen',

    ];
    
    public function category()
    {
       /*  return $this->belongsTo(Category::class); */
 /*       return $this->hasOne('App\Models\Category', 'id'); */
       
    }

    public function comment()
    {
        return $this->hasMany('App\Models\Comment', 'category_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course()
    {
        return $this->hasMany('App\Models\Course', 'category_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function forum()
    {
        return $this->hasMany('App\Models\Forum', 'category_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function post()
    {
        return $this->hasMany('App\Models\Post', 'category_id', 'id');
    }
    

}
